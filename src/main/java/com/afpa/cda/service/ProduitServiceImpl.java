package com.afpa.cda.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.ProduitDao;
import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.entity.Produit;

@Service
public class ProduitServiceImpl implements IProduitService {

	@Autowired
	private ProduitDao produitDao;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public List<ProduitDto> findAll() {
		return produitDao.findAll()
				.parallelStream()
				.map(p->this.modelMapper.map(p,ProduitDto.class))
				.collect(Collectors.toList());
	}
	
	@Override
	public void add(ProduitDto pDto) {
		pDto.setId(
				this.produitDao.save(this.modelMapper.map(pDto,Produit.class))
				.getId()
				);
	}

}
