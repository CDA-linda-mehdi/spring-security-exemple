package com.afpa.cda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.service.IProduitService;

@Controller
public class AccueilController {
	
	@Autowired
	private IProduitService produitService;
	
	@RequestMapping(path = {"/","list.html"}, method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView accueil(ModelAndView mv) {
		
		mv.addObject("produits", this.produitService.findAll());
		mv.addObject("nom", "Clement");
		mv.setViewName("list");
		
		return mv;
	}
	
	@GetMapping(path = {"login.html"})
	public ModelAndView login1(ModelAndView mv) {
		mv.setViewName("login");
		return mv;
	}
	
}
