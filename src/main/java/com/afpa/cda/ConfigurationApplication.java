package com.afpa.cda;

import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;

import com.afpa.cda.dao.ProduitDao;
import com.afpa.cda.entity.Produit;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class ConfigurationApplication {
	
	@Bean
	public ModelMapper createModelMapper() {
		return new ModelMapper();
	}

	@Bean
    public CommandLineRunner init (ProduitDao produitDao){
        return args -> {
            if(produitDao.count() == 0) {
            	log.info("table produits vide ! insertion ...");
            	produitDao.save(Produit.builder().label("télé").prix(99.99f).build());
            	produitDao.save(Produit.builder().label("vélo").prix(120f).build());
            	produitDao.save(Produit.builder().label("ps4").prix(350.5f).build());
            }
            produitDao.findAll(PageRequest.of(0, 10))
            	.stream()
            	.map(Produit::toString)
            	.forEach(log::info);
        };
    }
	
}
