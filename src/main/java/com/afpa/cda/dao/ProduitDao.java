package com.afpa.cda.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Produit;

@Repository
public interface ProduitDao extends PagingAndSortingRepository<Produit, Integer>{
	public List<Produit> findAll();
}
