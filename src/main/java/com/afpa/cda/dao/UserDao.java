package com.afpa.cda.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.afpa.cda.entity.User;

public interface UserDao extends JpaRepository<User, Integer> {

	Optional<User> findUserByName(String username);

}
